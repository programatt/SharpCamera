﻿// License:      GNU LGPLv3
// Author:       Benjamin N. Summerton <https://16bpp.net>
// Description:  Camera object.

using System;
using System.Runtime.InteropServices;
using System.Collections.Generic;

namespace libgphoto2Sharp
{
    public static partial class GPhoto2
    {
        #region PInvoke Functions
        [DllImport(GPhoto2DLL)]
        private static extern int gp_camera_new(ref IntPtr camera);

        [DllImport(GPhoto2DLL)]
        private static extern int gp_camera_free(IntPtr camera);

        [DllImport(GPhoto2DLL)]
        private static extern int gp_camera_init(IntPtr camera, IntPtr context);

        [DllImport(GPhoto2DLL)]
        private static extern int gp_camera_exit(IntPtr camera, IntPtr context);

        [DllImport(GPhoto2DLL)]
        private static extern int gp_camera_capture_preview(IntPtr camera, IntPtr file, IntPtr context);

        [DllImport(GPhoto2DLL)]
        private static extern int gp_camera_capture(IntPtr camera, CameraCaptureType type, out CameraFilePath path, IntPtr context);

        [DllImport(GPhoto2DLL)]
        private static extern int gp_camera_wait_for_event(
            IntPtr camera,
            int timeout,
            out CameraEventType eventtype,
            out IntPtr eventdata,               // Originally `void **`
            IntPtr context
        );

        // TODO replace sbyte with marshaled strings?
        [DllImport(GPhoto2DLL)]
        private static extern int gp_camera_file_get(
            IntPtr camera,
            sbyte[] folder,
            sbyte[] filename,
            CameraFileType type,
            IntPtr camera_file,
            IntPtr context
        );

        [DllImport(GPhoto2DLL)]
        private static extern int gp_camera_file_delete(IntPtr camera, sbyte[] folder, sbyte[] filename, IntPtr context);

        [DllImport(GPhoto2DLL)]
        private static extern int gp_camera_autodetect(IntPtr list, IntPtr context);

        [DllImport(GPhoto2DLL)]
        private static extern int gp_camera_get_port_info(IntPtr camera, out IntPtr info);

        [DllImport(GPhoto2DLL)]
        private static extern int gp_camera_set_port_info(IntPtr camera, IntPtr info);

        [DllImport(GPhoto2DLL, CallingConvention=CallingConvention.Cdecl, CharSet=CharSet.Ansi)]
        private static extern int gp_camera_get_single_config(
            IntPtr camera,
            [MarshalAs(UnmanagedType.LPStr)] string name,
            out IntPtr widget,
            IntPtr context
        );

        [DllImport(GPhoto2DLL, CallingConvention=CallingConvention.Cdecl, CharSet=CharSet.Ansi)]
        private static extern int gp_camera_set_single_config(
            IntPtr camera,
            [MarshalAs(UnmanagedType.LPStr)] string name,
            IntPtr widget,
            IntPtr context
        );

        [DllImport(GPhoto2DLL)]
        private static extern int gp_camera_get_config(IntPtr camera, out IntPtr window, IntPtr context);
        #endregion

        public class Camera : IDisposable {
            private GPContext context;
            private CameraFile camFile;
            private bool disposed = false;
            public IntPtr handle;    // Camera *

            public Camera() {
                context = new GPContext();
                camFile = new CameraFile();

                // Create camera object
                int ok = gp_camera_new(ref handle);
                if (ok != GP_OK)
                    throw new GPhoto2Exception("Couldn't create new Camera", ok);
            }

            ~Camera() {
                Dispose(false);
            }

            public void Dispose() {
                Dispose(true);
                GC.SuppressFinalize(this);
            }

            /// Does the actual cleanup work
            protected virtual void Dispose(bool disposing)
            {
                if (disposed)
                    return;

                Exit();

                // Free Managed Resources
                if (disposing)
                {
                    camFile.Dispose();
                    context.Dispose();
                }

                // Free Unmanaged resources
                gp_camera_free(handle);

                disposed = true;
            }

            /// <summary>
            /// Sets up the connection to the camera.  You do not need to create a GPContext, as there is one under the
            /// hood in this class.
            /// </summary>
            public void Init() {
                // Set context to camera
                int ok = gp_camera_init(handle, context.handle);
                if (ok != GP_OK)
                    throw new GPhoto2Exception("Couldn't set context to Camera and connect", ok);
            }

            /// <summary>
            /// Closes the connection to the camera, if there is one setup.
            /// </summary>
            public int Exit() {
                if (context != null)
                    return gp_camera_exit(handle, context.handle);
                else
                    return GP_OK;
            }


            /// <summary>
            /// Capture a preview image from the camera
            /// </summary>
            /// <returns>The preview.</returns>
            public byte[] CapturePreview()
            {
                int ok = gp_camera_capture_preview(handle, camFile.handle, context.handle);
                if (ok != GP_OK)
                    throw new GPhoto2Exception("Not able to capture a preview image from camera", ok);

                return camFile.Get();
            }

            /// <summary>
            /// Get a full image capture from the Camera.  This will create a new capture of an image and clean it up
            /// on the camera.  This does more than the original `gp_camera_capture()` function.
            /// </summary>
            public byte[] Capture()
            {
                CameraFilePath path;
                int ok = gp_camera_capture(handle, CameraCaptureType.GP_CAPTURE_IMAGE, out path, context.handle);
                if (ok != GP_OK)
                    throw new GPhoto2Exception("Not able to capture image from camera", ok);

                // Get it!
                FileGet(path);                  // Store image data
                byte[] image = camFile.Get();   // Pull it out
                FileDelete(path);               // Clean up on camera

                return image;
            }

            /// <summary>
            /// This will (try) to wait for an event from the Camera for a set amount of time.null
            ///
            /// Note that for the `Item2` field of the return value, you may beed to call `Marshal.FreeHGlobal()`
            /// to clean up any leftover (non-GC'd) memory from the native library.  This all depends on the value
            /// of `Item1` in the return value.
            ///
            /// Please see the documentation of the native C library for details:
            ///   http://gphoto.org/doc/api/gphoto2-camera_8c.html#a6a3aa30acd4bbe74087ce0fbcb9ea3a9
            ///
            /// Also please take a look at how this method is used in the `sample-trigger-capture.c` example
            /// program:
            ///   https://github.com/gphoto/libgphoto2/blob/d8fda3263c35da576fbcbf15e6c287204cefdbe9/examples/sample-trigger-capture.c
            /// </summary>
            /// <param name="timeout">time to wait in milliseconds</param>
            /// <returns>First item is the type of event that happened, second is the data for the event (if any)</returns>
            public Tuple<CameraEventType, IntPtr> WaitForEvent(int timeout)
            {
                CameraEventType eventType;
                IntPtr data = IntPtr.Zero;

                int ok = gp_camera_wait_for_event(handle, timeout, out eventType, out data, context.handle);
                if (ok != GP_OK)
                    throw new GPhoto2Exception("Error waiting for camera event", ok);

                return new Tuple<CameraEventType, IntPtr>(eventType, data);
            }

            public GPPortInfo PortInfo
            {
                get
                {
                    IntPtr info;
                    int ok = gp_camera_get_port_info(handle, out info);
                    if (ok != GP_OK)
                        throw new GPhoto2Exception("Failture trying to get the port info from the camera", ok);

                    return new GPPortInfo(info);
                }

                set
                {
                    int ok = gp_camera_set_port_info(handle, value.handle);
                    if (ok != GP_OK)
                        throw new GPhoto2Exception("Failture trying to set the port info for the camera", ok);
                }
            }

            // TODO returns the internally used file.  Is that good?  Or should iut return a copy instead?
            /// <summary>
            /// Retrive a file from the camera.
            /// </summary>
            /// <returns>A CameraFile object.  this is a pointer to the interally used one.  Do no dispose of it</returns>
            /// <param name="path">path where the file is located.</param>
            public CameraFile FileGet(CameraFilePath path)
            {
                int ok = gp_camera_file_get(handle, path.folder, path.name, CameraFileType.GP_FILE_TYPE_NORMAL, camFile.handle, context.handle);
                if (ok != GP_OK)
                    throw new GPhoto2Exception("Not able to get file from camera", ok);

                return camFile;
            }

            public void FileDelete(CameraFilePath path)
            {
                int ok = gp_camera_file_delete(handle, path.folder, path.name, context.handle);
                if (ok != GP_OK)
                    throw new GPhoto2Exception("Not able to delete file from camera", ok);
            }

            /// <summary>
            /// Get the widget configuration tree of the camera
            /// </summary>
            /// <value>Camera widgets,</value>
            public WindowCameraWidget Config
            {
                get
                {
                    IntPtr window;
                    int ok = gp_camera_get_config(handle, out window, context.handle);
                    if (ok != GP_OK)
                        throw new GPhoto2Exception("Not able to get root config widget", ok);

                    return new WindowCameraWidget(window);
                }
            }

            /// <summary>
            /// Retrive a configuration widget by name
            /// </summary>
            /// <returns>CameraWidget that can be configured.  Note that this will probably be one of the possible
            /// widget subclasses, such as RadioCameraWidget.  You can determine this by checking the `Type` property.
            /// </returns>
            /// <param name="name">name of the widget, e.g. "shutterspeed", or "iso", etc.</param>
            public CameraWidget GetSingleConfig(string name)
            {
                IntPtr widgetPtr;

                int ok = gp_camera_get_single_config(handle, name, out widgetPtr, context.handle);
                if (ok == GP_ERROR_BAD_PARAMETERS)
                    throw new WidgetNotFoundException(name);    // Assume it's a bad widget name
                else if (ok != GP_OK)
                    throw new GPhoto2Exception("Not able to retrive widget from camera", ok);

                // Depending upon the type, it's needs to be something else
                return CameraWidget.NewFromType(widgetPtr);
            }

            /// <summary>
            /// Set a single configuration onto the camera
            /// </summary>
            /// <param name="widget">Widget to alter</param>
            public void SetSingleConfig(CameraWidget widget)
            {
                int ok = gp_camera_set_single_config(handle, widget.Name, widget.handle, context.handle);
                if (ok != GP_OK)
                    throw new GPhoto2Exception("Error setting the widget config on the camera", ok);
            }



//            public static List<Camera> AutoDetect(GPContext context) {
//                CameraList list = new CameraList();
//                int ok = gp_camera_autodetect(list.handle, context.handle);
//
//                if (ok != GP_OK)
//                    throw new GPhoto2Exception("Failure trying to auto-detect cameras", ok);
//
//                Console.WriteLine("(Auto)Detected {0} cameras.", list.Count);
//
//                // Else, we've got some cameras!, but them in a more C# friendly form
//                List<Camera> cams = new List<Camera>();
//                return cams;
//            }
        }
    }
}

