﻿// License:      GNU LGPLv3
// Author:       Benjamin N. Summerton <https://16bpp.net>
// Description:  Driver Status

using System;

namespace libgphoto2Sharp
{
    public static partial class GPhoto2
    {
        public enum CameraDriverStatus
        {
            GP_DRIVER_STATUS_PRODUCTION,    // Driver is production ready.
            GP_DRIVER_STATUS_TESTING,       // Driver is beta quality.
            GP_DRIVER_STATUS_EXPERIMENTAL,  // Driver is alpha quality and might even not work.
            GP_DRIVER_STATUS_DEPRECATED     // Driver is no longer recommended to use and will be removed.
        }
    }
}

