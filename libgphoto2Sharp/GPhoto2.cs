﻿// License:      GNU LGPLv3
// Author:       Benjamin N. Summerton <https://16bpp.net>
// Description:  C# friendly bindings to libgphoto2.

using System;

namespace libgphoto2Sharp
{
    /// <summary>
    /// C# friendly bindings to libgphoto2.
    /// </summary>
    public static partial class GPhoto2
    {
        public const string GPhoto2DLL = "gphoto2";
    }
}
