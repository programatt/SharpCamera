﻿// License:      GNU LGPLv3
// Author:       Benjamin N. Summerton <https://16bpp.net>
// Description:  Handy little methods for binding libgphoto2 and making it C# friendly.

using System;

namespace libgphoto2Sharp
{
    // Utilties for this binding.
    public static class Utils
    {
        // Adapted from: https://www.safaribooksonline.com/library/view/c-cookbook/0596003390/ch16s10.html
        public static String SByteArrayToString(sbyte[] bytes)
        {
            String str;

            unsafe
            {
                fixed(sbyte* ptr = bytes)
                {
                    str = new string(ptr);
                }
            }

            return (str);
        }

        public static readonly DateTime UnixEpoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);

        /// <summary>
        /// Convert a UNIX Timestamp to a C# DateTime object
        /// </summary>
        /// <returns>A DateTime object (UTC)</returns>
        /// <param name="seconds">Seconds since that famous epoch.</param>
        public static DateTime UnixTimestampToDateTime(uint seconds)
        {
            return UnixEpoch.AddSeconds(seconds);
        }

        public static uint DateTimeToUnixTimestamp(DateTime dt)
        {
            return (uint)(dt - UnixEpoch).TotalSeconds;
        }
    }
}

