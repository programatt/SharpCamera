﻿// License:      GNU LGPLv3
// Author:       Benjamin N. Summerton <https://16bpp.net>
// Description:  A CameraWidget that is of the Text type
using System;
using System.Runtime.InteropServices;

namespace libgphoto2Sharp
{
    public static partial class GPhoto2
    {
        /// <summary>
        /// A Camera widget that is simply text.
        /// </summary>
        public class TextCameraWidget : CameraWidget
        {
            /// <summary>
            /// Create a widget that is simply text, from a previously instatited object.
            /// </summary>
            /// <param name="widget">handle to a Text widget.</param>
            public TextCameraWidget(IntPtr widget)
                : base(widget)
            {
                if (Type != CameraWidgetType.GP_WIDGET_TEXT)
                    throw new ArgumentException("Supplied handle is not a Text widget");
            }

            /// <summary>
            /// Create a new Text widget
            /// </summary>
            /// <param name="label">Label to give to the menu.</param>
            public TextCameraWidget(string label)
            {
                int ok = gp_widget_new(CameraWidgetType.GP_WIDGET_TEXT, label, out handle);
                if (ok != GP_OK)
                    throw new GPhoto2Exception("error creating new Text widget", ok);
            }


            /// <summary>
            /// Get/Set the value of the Text widget.
            /// </summary>
            /// <value>A String</value>
            public string Value
            {
                get
                {
                    IntPtr valuePtr = IntPtr.Zero;
                    int ok = gp_widget_get_value(handle, out valuePtr);
                    if (ok != GP_OK)
                        throw new GPhoto2Exception("Error getting widget text", ok);

                    return Marshal.PtrToStringAnsi(valuePtr);
                }

                set
                {
                    // Try to set it
                    IntPtr valueStr = Marshal.StringToHGlobalAnsi(value);
                    int ok = gp_widget_set_value(handle, valueStr);
                    if (ok != GP_OK)
                        throw new GPhoto2Exception("Error setting widget text", ok);

                    // Cleanup
                    Marshal.FreeHGlobal(valueStr);
                }
            }
        }
    }
}

